<?php
echo $this->Html->css(['/vendor/bootstrap/dist/css/bootstrap.min.css', 'IconicSeo.seo_plugin.css']);
?>
<br/>
<?=$this->Form->create($tag);?>

<div class="row container">

    <div class="row">
        <div class="col-xs-3">
            <?=$this->Form->label('attribute1_title', 'Title 1')?>
            <?php
            //to edit these array, must also edit in SeoEscapeTrait
            $options = ['name' => 'name', 'charset' => 'charset', 'property' => 'property',
                'itemprop' => 'itemprop', 'http-equiv' => 'http-equiv'];
            echo $this->Form->select('attribute1_title', $options, [
                'default' => !empty($tag->attribute1_title) ? $tag->attribute1_title : 'name',
                'class'     => 'form-control',
                'autofocus' => TRUE
            ]);?>
        </div>
        <div class="col-xs-9">
            <?=$this->Form->label('attribute1_content', 'Content 1')?>
            <?php
            //to edit these array, must also edit in SeoEscapeTrait
            $options = ['keywords' => 'keywords', 'description' => 'description', 'UTF-8' => 'UTF-8',
                'content-type' => 'content-type', 'author' => 'author', 'og:locale' => 'og:locale',
                'og:description' => 'og:description', 'og:site_name' => 'og:site_name',
                'og:title' => 'og:title', 'og:type' => 'og:type', 'og:image' => 'og:image', 'og:url' => 'og:url',
                'robots'    => 'robots'
            ];
            echo $this->Form->select('attribute1_content', $options, [
                'default' => !empty($tag->attribute1_content) ? $tag->attribute1_content : 'description',
                'class'     => 'form-control'
            ]);?>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-xs-3">
            <?=$this->Form->label('attribute2_title', 'Title 2')?>
            <?=$this->Form->input('attribute2_title', [
                'default'   => !empty($tag->attribute2_title) ? $tag->attribute2_title : 'content',
                'label'     => false,
                'required'  => TRUE,
                'autofocus' => TRUE,
                'class'     => 'form-control'
            ]);?>
        </div>
        <div class="col-xs-9">
            <?=$this->Form->label('attribute2_content', 'Content 2')?>
            <?=$this->Form->textarea('attribute2_content', [
                'default'   => !empty($tag->attribute2_content) ? $tag->attribute2_content : '',
                'label'     => false,
                'required'  => TRUE,
                'class'     => 'form-control'
            ]);?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12" style="text-align: center; margin-top: 20px;">
            <?=$this->Html->link(
                'Cancel',
                ['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'edit', $tag->seo_uri_id],
                ['confirm' => 'Are you sure you wish to cancel?'])
            ?> &nbsp;
            <button type="submit" class="btn btn-md btn-primary" value="save" id="save">SAVE CHANGE</button>
        </div>
    </div>

</div>

<?=$this->Form->end();?>