<?php
use Cake\Routing\Router;
?>
<div class="col-xs-12 table-responsive">
    <table data-order='[[0, "asc"]]' data-page-length='25' id="seo-list" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>URI</th>
            <th></th>
            <th></th>
        </tr>
        </thead>

        <tbody>
        <?php
        /** @var \Seo\Model\TagBundleInterface[] $seo_bundles */
        foreach ($seo_bundles as $seo_bundle) {
            $display_uri = str_replace(PROTOCOL . "://" . $_SERVER["HTTP_HOST"], '', $seo_bundle->getUri());
            ?>
            <tr>
                <td>
                    <div class="data-column">
                        <a class="uri-link-preview" target="_blank" href="<?=$seo_bundle->getUri()?>"><?=$display_uri?>/</a>
                        <?=nl2br(htmlspecialchars($seo_bundle->renderHead()))?>
                    </div>
                </td>
                <td><a href="<?=Router::url(['plugin' => 'IconicSeo', 'controller' => 'NewUris', 'action' => 'edit', $seo_bundle->getId()])?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td><span id="delete-<?=$seo_bundle->getId()?>" class="glyphicon glyphicon-remove"></span></td>
            </tr>
        <?php } ?>

        </tbody>
    </table>

</div>

<?php
$this->start('script');
echo $this->fetch('script'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        var seo_list_table = $('#seo-list').DataTable();

        $('#seo-list tbody').on( 'click', '.glyphicon.glyphicon-remove', function () {
            if (confirm("Are you sure you wish DELETE this link?")) {

                var id = $(this).attr('id');
                id = id.replace('delete-', '');
                var response =  $.ajax({
                    url: '/iconic_seo/NewUris/deleteAjax',
                    type: 'POST',
                    dataType: 'html',
                    data: 'id=' + id,
                    global: false,
                    async:false,
                    success: function (html) {
                        return html;
                    }
                }).responseText;

                if(response === '1'){
                    seo_list_table
                        .row( $(this).parents('tr') )
                        .remove()
                        .draw();
                } else {
                    alert('Cannot delete this Uri.');
                }
            }
            return false;
        } );

    } );
</script>
<?php $this->end();?>
