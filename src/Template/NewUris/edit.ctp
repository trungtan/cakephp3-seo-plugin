<div class="container col-xs-12">
    <?=$this->Form->create($tag_bundle)?>

<?php
use Seo\Utility\ParserOptions;

/** @var \Seo\Model\TagBundleInterface $tag_bundle $count */

echo $tag_bundle->getUri();
$count = count($tag_bundle->getMetaTags());
for ($i = 0; $i < $count; $i++) {
    /** @var \Seo\Model\TagAbstract $tag */
    $tag = $tag_bundle->getMetaTags()[$i];
    ?>
    <div class="row one-tag" id="tag-<?=$i?>">
        <?=$this->Form->hidden("tags[{$i}][type]", ['value'=>$tag->getType()])?>

        <?=$tag_types_options[$tag->getType()]?>
        <?=$this->Form->select(
            "tags[{$i}][language_code]", $language_options, [
                'default' => $tag->getLanguageCode()
            ]
        )?>

        <?php
        $j = 0;
        foreach ((array)$tag->getAttributes() as $key=>$value) {
            $j++;
            echo $this->Form->text(
                    "tags[{$i}][attributes][{$j}][key]", [
                        'value' => $key,
                        'size' => 10
                    ]
                ) . ' = ' ;
            echo $this->Form->text(
                "tags[{$i}][attributes][{$j}][value]", [
                    'value' => $value,
                    'size' => 30
                ]
            );
        }
        ?>

        <?php
        if ( $tag->getType() == \Seo\Model\TagAbstract::TEXT_TAG || $tag->getType() == \Seo\Model\TagAbstract::HTML_TAG) {
            echo $this->Form->text(
                "tags[{$i}][tag_name]", [
                    'value' => $tag->getTagName()
                ]
            );
        }
        ?>

        <?php
        if (!empty($tag->getInnerText())) {
            echo $this->Form->textarea(
                "tags[{$i}][inner_text]", [
                    'value' => $tag->getInnerText(),
                    'rows' => 1,
                    'cols' => 92
                ]
            );
        }
        ?>
        <span class="glyphicon glyphicon-trash" id="btn-delete-<?php echo $i?>"></span>
    </div>

<?php }?>

    <div class="col-xs-12 text-center">
        <?php
        echo $this->Form->button('SAVE');
        ?>
        <span class="btn btn-default">Add Tag</span>
    </div>

    <?php echo $this->Form->end();?>
</div>


<?php
$this->start('script');
echo $this->fetch('script'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.glyphicon.glyphicon-trash').click(function(){
            var id = $(this).attr('id');
            id = id.replace('btn-delete-', '');
            var row = $('#tag-' + id);
            row.animate({
                height: "toggle"
            }, 100, function () {
                row.remove();
            });
        });
    } );
</script>
<?php $this->end();?>
