<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $page_title ?>
    </title>

    <?php
        echo $this->Html->css(
            [
                '/iconic_seo/vendor/bootstrap/dist/css/bootstrap.min.css',
                '/iconic_seo/vendor/datatables/media/css/jquery.dataTables.min.css',
                'IconicSeo.layout.css', 'IconicSeo.module.css',
                'IconicSeo.state.css', 'IconicSeo.seo_plugin_old.css',
            ]
        );
        echo $this->Html->script([
            '/iconic_seo/vendor/jquery/dist/jquery.min',
            '/iconic_seo/vendor/bootstrap/dist/js/bootstrap.min',
            '/iconic_seo/vendor/datatables/media/js/jquery.dataTables.min.js',
            'IconicSeo.script'
        ]);
    ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>

<div class="container-fluid l-header">
    <div class="row">
        <div class="col-sm-9 header-title"><?=$page_title?></div>
        <div class="col-sm-3 header-account">
            <a class="label label-info" href="<?=$this->Url->build(['plugin' => NULL, "controller" => "Employers"]);?>">Back</a>
            <a class="label label-info" href="<?=$this->Url->build(['plugin' => NULL, "controller" => "users","action" => "logout"]);?>">Log out</a>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <?= $this->Flash->render(); ?>
        </div>
    </div>

    <div class="row">
        <?= $this->fetch('content') ?>
    </div>
</div>

<footer>

</footer>

</body>
</html>
