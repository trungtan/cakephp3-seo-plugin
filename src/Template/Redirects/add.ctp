<?php
echo $this->Html->css(['/vendor/bootstrap/dist/css/bootstrap.min.css', 'IconicSeo.seo_plugin.css']);
?>
<br/>
<?=$this->Form->create($redirect);?>

<div class="row container">
    <div class="row">
        <div class="col-xs-9">
            <?=$this->Form->label('redirect', 'Target URL (to redirect to)')?>
            <?=$this->Form->input('redirect', [
                'default'   => !empty($redirect->redirect) ? $redirect->redirect : '',
                'label'     => false,
                'required'  => TRUE,
                'autofocus' => TRUE,
                'class'     => 'form-control'
            ]);?>
        </div>

        <div class="col-xs-1">
            <?=$this->Form->label('type', 'Type')?>
            <?php
            $options = [301 => '301', 302 => '302'];
            $default_status = 302;
            if(isset($redirect->type)){
                $default_status = $redirect->type;
            }
            echo $this->Form->select('type', $options, [
                'default' => $default_status,
                'class'     => 'form-control'
            ]);?>
        </div>

        <div class="col-xs-2">
            <?=$this->Form->label('is_active', 'Status')?>
            <?php
            $options = [0 => __('Disabled'), 1 => __('Active')];
            $default_status = 0;
            if(isset($request->is_active)){
                $default_status = $request->is_active;
            }
            echo $this->Form->select('is_active', $options, [
                'default' => $default_status,
                'class'     => 'form-control'
            ]);?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12" style="text-align: center; margin-top: 20px;">
            <?=$this->Html->link(
                'Cancel',
                ['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'edit', $redirect->seo_uri_id],
                ['confirm' => 'Are you sure you wish to cancel?'])
            ?> &nbsp;
            <button type="submit" class="btn btn-md btn-primary" value="save" id="save">SAVE CHANGE</button>
        </div>
    </div>

</div>

<?=$this->Form->end();?>