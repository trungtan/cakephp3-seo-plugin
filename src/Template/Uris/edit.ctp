<?php
echo $this->Html->css(['/vendor/bootstrap/dist/css/bootstrap.min.css', 'IconicSeo.seo_plugin.css']);
?>
<br/>
<div class="row container" id="edit-uri">
    <div class="row">
        <div class="col-xs-12">
            <?= $this->Flash->render()?>
        </div>
    </div>

    <br/>

    <?=$this->Form->create($uri);?>
    <div class="row">
        <div class="col-xs-12 col-sm-5">
            <?=$this->Form->label('title.title', 'Page Title')?>
            <?=$this->Form->input('title.title', [
                'default'   => !empty($uri->title) ? $uri->title->title : '',
                'label'     => false,
                  'required'  => TRUE,
                'class'     => 'form-control',
            ]);?>
        </div>

        <div class="col-xs-10 col-sm-5">
            <?=$this->Form->label('canonical.canonical', 'Canonical Link')?>
            <?=$this->Form->input('canonical.canonical', [
                'default'   => !empty($uri->canonical) ? $uri->canonical->canonical: '',
                'label'     => false,
                'required'  => TRUE,
                'class'     => 'form-control',
            ]);?>
        </div>
        <div class="col-xs-2 col-sm-2">
            <?=$this->Form->label('canonical.is_active', 'Status')?>
            <?php
            $options = [1 => __('Active'), 0 => __('Disabled')];
            $default_status = 1;
            if(isset($uri->canonical->is_active) && $uri->canonical->is_active == 0){
                $default_status = 0;
            }
            echo $this->Form->select('canonical.is_active', $options, [
                'default' => $default_status,
                'class'     => 'form-control'
            ]);?>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-xs-10 col-sm-4">
            <?=$this->Form->label('top_message.top_message', 'Top message(JA)')?>
            <?=$this->Form->input('top_message.ja_top_message', [
                //'default'   => !empty($uri->top_message) ? $uri->top_message->ja_top_message: '',
                'label'     => false,
                'class'     => 'form-control',
            ]);?>
        </div>
        <div class="col-xs-10 col-sm-4">
            <?=$this->Form->label('top_message.top_message', 'Top message(EN)')?>
            <?=$this->Form->input('top_message.en_top_message', [
                //'default'   => !empty($uri->top_message) ? $uri->top_message->en_top_message: '',
                'label'     => false,
                'class'     => 'form-control',
            ]);?>
        </div>
        <div class="col-xs-10 col-sm-4">
            <?=$this->Form->label('top_message.top_message', 'Top message(VI)')?>
            <?=$this->Form->input('top_message.vi_top_message', [
                //'default'   => !empty($uri->top_message) ? $uri->top_message->vi_top_message: '',
                'label'     => false,
                'class'     => 'form-control',
            ]);?>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <?=$this->Form->label('footer_keyword.ja_keywords', 'Footer keywords(JA)')?>
            <?=$this->Form->input('footer_keyword.ja_keywords', [
                'label'     => false,
                'class'     => 'form-control',
                'rows'       =>'1'
            ]);?>
        </div>
        <div class="col-xs-12 col-sm-4">
            <?=$this->Form->label('footer_keyword.en_keywords', 'Footer keywords(EN)')?>
            <?=$this->Form->input('footer_keyword.en_keywords', [
                'label'     => false,
                'class'     => 'form-control',
                'rows'       =>'1'
            ]);?>
        </div>
        <div class="col-xs-12 col-sm-4">
            <?=$this->Form->label('footer_keyword.vi_keywords', 'Footer keywords(VI)')?>
            <?=$this->Form->input('footer_keyword.vi_keywords', [
                'label'     => false,
                'class'     => 'form-control',
                'rows'       =>'1'
            ]);?>
        </div>
    </div>
    <br/>
    <div class="row">

    </div>
    <br/>
    <div class="row">

    </div>
    <br/>

    <div class="row">
        <?php if(!empty($uri->redirect)){ ?>
        <div class="col-xs-9">
            <?=$this->Form->label('redirect.redirect', 'Redirect Link')?>
            <?=$this->Html->link(
                '<span class="label label-danger">Delete</span>',
                ['plugin' => 'IconicSeo', 'controller' => 'Redirects', 'action' => 'delete', $uri->redirect->id],
                ['confirm' => 'Are you sure you wish to cancel?', 'escape' => false]
            )?>
            <?=$this->Form->input('redirect.redirect', [
                'default'   => $uri->redirect->redirect,
                'label'     => false,
                'required'  => TRUE,
                'class'     => 'form-control',
            ]);?>
        </div>
        <div class="col-xs-1">
            <?=$this->Form->label('redirect.type', 'Type')?>
            <?php
            $options = [301 => '301', 302 => '302'];
            $default_status = 302;
            if(isset($uri->redirect)){
                $default_status = $uri->redirect->type;
            }
            echo $this->Form->select('redirect.type', $options, [
                'default' => $default_status,
                'class'     => 'form-control'
            ]);?>
        </div>

        <div class="col-xs-2">
            <?=$this->Form->label('redirect.is_active', 'Status')?>
            <?php
            $options = [0 => __('Disabled'), 1 => __('Active')];
            $default_status = 0;
            if(isset($request->is_active)){
                $default_status = $request->is_active;
            }
            echo $this->Form->select('redirect.is_active', $options, [
                'default' => $default_status,
                'class'     => 'form-control'
            ]);?>
        </div>
        <?php } else { ?>
            <div class="col-xs-12" style="font-size: 16px">
                <?=$this->Html->link(
                    '<span class="label label-default">Add redirection</span>',
                    ['plugin' => 'IconicSeo', 'controller' => 'Redirects', 'action' => 'add', $uri->id],
                    ['escape' => false]
                )?>
            </div>
        <?php }?>
    </div>
    <br/>

    <?php if(!empty($uri->meta_tags)){?>
        <div class="row">
            <div class="col-xs-12" style="font-size: 18px; margin-bottom: 10px;">
                <?=$this->Html->link(
                    '<span class="label label-default">Add</span>',
                    ['plugin' => 'IconicSeo', 'controller' => 'MetaTags', 'action' => 'add', $uri->id],
                    ['escape' => false]
                )?>
                <b>Meta Tags</b>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php foreach($uri->meta_tags as $tag) { ?>
                    <?php
                        echo $this->Html->link(
                            '<span class="label label-info">Edit</span>',
                            ['plugin' => 'IconicSeo', 'controller' => 'MetaTags', 'action' => 'edit', $tag->id],
                            ['escape' => false]) . " ";
                        echo $this->Html->link(
                            '<span class="label label-danger">Delete</span>',
                            ['plugin' => 'IconicSeo', 'controller' => 'MetaTags', 'action' => 'delete', $tag->id],
                            ['confirm' => 'Are you sure you wish to delete?', 'escape' => false]);
                    ?>
                    <?=h("<meta {$tag->attribute1} {$tag->attribute2}>") . "<br/><br/>";?>
                <?php }?>
            </div>
        </div>
    <?php } ?>



    <div class="row">
        <div class="col-xs-12" style="text-align: center; margin-top: 20px;">
            <?=$this->Html->link(
                'Cancel',
                ['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index'],
                ['confirm' => 'Are you sure you wish to cancel?'])
            ?> &nbsp;
            <button type="submit" class="btn btn-md btn-primary" value="save" id="save">SAVE CHANGE</button>
        </div>
    </div>
    <?=$this->Form->end();?>


</div>