<?php
use Cake\Routing\Router;
echo $this->Html->css(['IconicSeo.seo_plugin.css']);
?>
<div class="block">
    <header>
        <h2>SEO Management</h2>
    </header>
    <section>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-filter">
                    Sort:
                    <?= $this->Html->link(__('All'), [
                        'plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index']);
                    ?> |
                    <?= $this->Html->link(__('Created'), [
                        'plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index', 'order' => 'created']);
                    ?> |
                    <?= $this->Html->link(__('Modified'), [
                        'plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index', 'order' => 'modified']);
                    ?> &nbsp;&nbsp;&nbsp;
                    <div class="search-url-form">
                        <?=$this->Form->create(null, ['type' => 'get'], ['url' => ['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index']]);?>
                        <input type="submit" value="Search:"/>
                        <input type="text" name="kw" id="kw" />
                        <?=isset($kw) ? 'Search result: <b>' . h($kw) . '</b>' : ''?>
                        <?= $this->Form->end(); ?>
                    </div>
                </div>

                <div class="page-list">
                    <?= '<ul><li><a href="#">' . $this->Paginator->counter() . '</a></li>' . $this->Paginator->first('<<') .
                    $this->Paginator->prev('<') . $this->Paginator->numbers(['modulus' => 4]) .
                    $this->Paginator->next('>') . $this->Paginator->last('>>') . '</ul>';?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="page-filter">
                    <a href="<?=$this->Url->build(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'convert'])?>" target="_blank" class="link">
                        <span class="label label-default">CONVERT</span>
                    </a>
                    Categories:
                    <a href="<?=$this->Url->build(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index', 'filter' => 'jobs_dir'])?>">
                        <?=__('Job List')?>
                    </a> |
                    <a href="<?=$this->Url->build(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index', 'filter' => 'static_pages'])?>">
                        <?=__('Static Pages')?>
                    </a> |
                    <a href="<?=$this->Url->build(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index', 'filter' => 'other'])?>">
                        <?=__('Others')?>
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <?php foreach($all_uris as $uri) { ?>
                    <table class="table table-bordered table-condensed url-info">
                        <thead>
                        <tr class="success">
                            <th width="66%">
                                <?=$this->Html->link(
                                    '<div class="label label-default">Edit</div>',
                                    ['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'edit', $uri->id],
                                    ['escape' => false]
                                )?>
                                <a name="uri_<?=$uri->id?>" href="http://<?=SITE_CANDIDATE_URL.$uri->uri?>" target="_blank"><?=$uri->uri?></a>
                            </th>
                            <th width="4%">
                                <?php
                                echo $this->Html->link(
                                    'Delete',
                                    Router::url(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'deleteUri', 'id' => $uri->id, 'page' => $this->Paginator->current()]),
                                    ['confirm' => 'Are you sure you wish DELETE this link?', 'class' => 'label label-danger']);
                                ?>
                            </th>
                            <th width="12%" title="created datetime"><?= !empty($uri->created) ? $uri->created->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                            <th width="12%" title="modified datetime"><?= !empty($uri->modified) ? $uri->modified->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($uri->redirect->redirect)){ ?>
                            <tr>
                                <td>
                                    <div class="label label-warning title-lv-1">Redirect</div>
                                    <?=$uri->redirect->redirect?>
                                    <div class="label label-default"><?=$uri->redirect->type?></div>
                                </td>
                                <td><?=$uri->redirect->is_active == 1 ? '<span class="label label-danger">Active</span>' : '<span class="label label-info">Disabled</span>'?></td>
                                <td><?= !empty($uri->redirect->created) ? $uri->redirect->created->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                                <td><?= !empty($uri->redirect->modified) ? $uri->redirect->modified->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                            </tr>
                        <?php }?>
                        <?php if(!empty($uri->title->title)){ ?>
                            <tr>
                                <td colspan="2">
                                    <div class="label label-info title-lv-1">Title</div>
                                    <?= $uri->title->title?></td>
                                <td><?= !empty($uri->title->created) ? $uri->title->created->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                                <td><?= !empty($uri->title->modified) ? $uri->title->modified->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                            </tr>
                        <?php }?>

                        <?php if(!empty($uri->canonical->canonical)){ ?>
                            <tr>
                                <td colspan="2">
                                    <div class="label label-info title-lv-1">Canonical</div>
                                    <?=$uri->canonical->canonical?></td>
                                <td><?= !empty($uri->canonical->created) ? $uri->canonical->created->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                                <td><?= !empty($uri->canonical->modified) ? $uri->canonical->modified->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                            </tr>
                        <?php }?>
                        <?php if(!empty($uri->top_message->ja_top_message)){ ?>
                            <tr>
                                <td colspan="2">
                                    <div class="label label-info title-lv-1">
                                        Top Message(JA)
                                    </div>
                                    <div style="display: inline-block; padding-left: 20px;">
                                        <?=$uri->top_message->ja_top_message?>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="label label-info title-lv-1">
                                        Top Message(EN)
                                    </div>
                                    <div style="display: inline-block; padding-left: 20px;">
                                        <?=$uri->top_message->en_top_message?>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="label label-info title-lv-1">
                                        Top Message(VI)
                                    </div>
                                    <div style="display: inline-block; padding-left: 20px;">
                                        <?=$uri->top_message->vi_top_message?>
                                    </div>


                                </td>

                                <td><?= !empty($uri->top_message->created) ? $uri->top_message->created->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                                <td><?= !empty($uri->top_message->modified) ? $uri->top_message->modified->i18nFormat('HH:mm MM.dd.YYYY') : ''?></th>
                            </tr>
                        <?php }?>
                        <?php if(!empty($uri->meta_tags)){ ?>
                            <tr>
                                <td colspan="4">
                                    <div class="label label-info title-lv-1">Meta Tags</div>
                                    <br/>
                                    <div class="title-lv-2">
                                        <?php foreach($uri->meta_tags as $tag) {
                                            echo h("<meta {$tag->attribute1} {$tag->attribute2}>") . "<br/>";
                                        }?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                <?php }?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="page-list">
                    <?= '<ul><li><a href="#">' . $this->Paginator->counter() . '</a></li>' . $this->Paginator->first('<<') .
                    $this->Paginator->prev('<') . $this->Paginator->numbers(['modulus' => 4]) .
                    $this->Paginator->next('>') . $this->Paginator->last('>>') . '</ul>';?>
                </div>
            </div>
        </div>
    </section>
</div>
