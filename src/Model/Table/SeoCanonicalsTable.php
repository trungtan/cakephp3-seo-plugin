<?php

namespace IconicSeo\Model\Table;

use Cake\ORM\Table;
use Cake\Core\Configure;

class SeoCanonicalsTable extends Table {

    public static function defaultConnectionName() {
        return !empty(Configure::read('private_additional')) ? Configure::read('private_additional') : 'additional';
    }

    public function initialize(array $config)
    {
        $this->table('seo_canonical');
        $this->entityClass('IconicSeo\Model\Entity\SeoCanonical');
    }
}
