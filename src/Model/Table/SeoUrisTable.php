<?php

namespace IconicSeo\Model\Table;

use Cake\ORM\Table;
use Cake\Core\Configure;

class SeoUrisTable extends Table {

    public static function defaultConnectionName() {
        return !empty(Configure::read('private_additional')) ? Configure::read('private_additional') : 'additional';
    }

    public function initialize(array $config)
    {
        $this->table('seo_uri');

        $this->hasOne('SeoTitles', [
            'className' => 'IconicSeo.SeoTitles',
            'foreignKey' => 'seo_uri_id',
            'dependent' => true,
            'propertyName' => 'title'
        ]);

        $this->hasOne('SeoCanonicals', [
            'className' => 'IconicSeo.SeoCanonicals',
            'foreignKey' => 'seo_uri_id',
            'dependent' => true,
            'propertyName' => 'canonical'
        ]);

        $this->hasMany('SeoMetaTags', [
            'className' => 'IconicSeo.SeoMetaTags',
            'foreignKey' => 'seo_uri_id',
            'dependent' => true,
            'propertyName' => 'meta_tags'
        ]);

        $this->hasOne('SeoRedirects', [
            'className' => 'IconicSeo.SeoRedirects',
            'foreignKey' => 'seo_uri_id',
            'dependent' => true,
            'propertyName' => 'redirect',
        ]);

        $this->hasOne('SeoTopMessages', [
            'className' => 'IconicSeo.SeoTopMessages',
            'foreignKey' => 'seo_uri_id',
            'dependent' => true,
            'propertyName' => 'top_message',
        ]);

        $this->hasOne('SeoFooterKeywords', [
            'className' => 'IconicSeo.SeoFooterKeywords',
            'foreignKey' => 'seo_uri_id',
            'dependent' => true,
            'propertyName' => 'footer_keyword',
        ]);

    }
}
