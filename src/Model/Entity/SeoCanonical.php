<?php
/**
 * Created by PhpStorm.
 * User: tanbt
 * Date: 05/06/2015
 * Time: 11:34
 */

namespace IconicSeo\Model\Entity;

use Cake\ORM\Entity;
use IconicSeo\Model\Behavior\Traits\SeoEscapeTrait;

class SeoCanonical extends Entity {
    use SeoEscapeTrait;

    protected function _getCanonical(){
        return isset($this->_properties['canonical']) ? $this->standardizeCanonicalLink($this->_properties['canonical']): '';
    }

}