<?php
/**
 * Created by PhpStorm.
 * User: tanbt
 * Date: 05/06/2015
 * Time: 09:25
 */

namespace IconicSeo\Model\Entity;

use Cake\ORM\Entity;
use IconicSeo\Model\Behavior\Traits\SeoEscapeTrait;

class SeoMetaTag extends Entity {

    use SeoEscapeTrait;

    /**
     * @return string
     * Example: name="description"
     */
    protected function _getAttribute1(){
        $return_str = '';
        if($this->checkAttribute1Title($this->_properties['attribute1_title'])){
            $return_str = "{$this->_properties['attribute1_title']}=";

            if($this->checkAttribute1Content($this->_properties['attribute1_content'])){
                $return_str .= "\"{$this->_properties['attribute1_content']}\"";
            } else {
                $return_str .= "\"\"";
            }
        }
        return $return_str;
    }

    /**
     * @return string
     * Example: content="Some of your content"
     */
    protected function _getAttribute2(){
        $return_str = '';

        if(!empty($this->_properties['attribute2_title'])){
            $return_str = "{$this->_properties['attribute2_title']}=";

            if(!empty($this->_properties['attribute2_content'])){
                $return_str .= "\"{$this->_properties['attribute2_content']}\"";
            } else {
                $return_str .= "\"\"";
            }
        }
        return $return_str;
    }

}