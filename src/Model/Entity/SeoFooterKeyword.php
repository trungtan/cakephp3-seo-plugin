<?php

namespace IconicSeo\Model\Entity;

use Cake\ORM\Entity;
use IconicSeo\Model\Behavior\Traits\SeoEscapeTrait;

class SeoFooterKeyword extends Entity {

    use SeoEscapeTrait;

}