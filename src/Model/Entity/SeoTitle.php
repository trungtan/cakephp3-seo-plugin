<?php
/**
 * Created by PhpStorm.
 * User: tanbt
 * Date: 05/06/2015
 * Time: 09:25
 */

namespace IconicSeo\Model\Entity;

use Cake\ORM\Entity;
use IconicSeo\Model\Behavior\Traits\SeoEscapeTrait;

class SeoTitle extends Entity {

    use SeoEscapeTrait;

    protected function _getTitle(){
        return isset($this->_properties['title']) ? $this->standardizeSeoTitle($this->_properties['title']) : '';
    }

}