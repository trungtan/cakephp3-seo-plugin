<?php

namespace IconicSeo\Model\Entity;

use Cake\ORM\Entity;
use IconicSeo\Model\Behavior\Traits\SeoEscapeTrait;

class SeoTopMessage extends Entity {

    use SeoEscapeTrait;

    protected function _getTopMessage(){
        return isset($this->_properties['top_message']) ? $this->standardizeSeoTitle($this->_properties['top_message']) : '';
    }

}