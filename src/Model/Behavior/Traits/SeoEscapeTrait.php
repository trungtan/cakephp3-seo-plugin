<?php
/**
 * Created by PhpStorm.
 * User: tanbt
 * Date: 05/06/2015
 * Time: 10:18
 */

namespace IconicSeo\Model\Behavior\Traits;

/**
 * Contains a preparation methods aimed to optimize output string for an SEO entities' contents.
 */

trait SeoEscapeTrait {

    public function standardizeSeoTitle($title){
        //Think about integrate more functions to correct the title follow SEO rule for title
        return $this->removeSpaces($title);
    }

    public function standardizeCanonicalLink($canonical){
        return $canonical;
    }

    private function removeSpaces($string){
        return trim(preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string));
    }

    /*
     * The allowed arrays should have a management.
     */
    public function checkAttribute1Title($title){
        $allowed = ['name', 'charset', 'property', 'itemprop', 'http-equiv'];
        return in_array($title, $allowed);
    }
    public function checkAttribute1Content($content){
        $allowed = ['keywords', 'description', 'UTF-8', 'content-type', 'author',
                    'og:locale', 'og:description', 'og:site_name', 'og:title', 'og:type', 'og:image', 'og:url', 'og:image:width',
                    'robots'
        ];
        return in_array($content, $allowed);
    }

}