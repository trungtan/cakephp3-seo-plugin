<?php
/**
 * Created by PhpStorm.
 * User: tanbt
 * Date: 12/06/2015
 * Time: 15:33
 */

namespace IconicSeo\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class MetaTagsController extends AppController{

    public function add($uri_id){
        $this->set('page_title', 'URIs SEO Management > URI > Meta Tags > Add');
        $meta_tag_table     = TableRegistry::get('IconicSeo.SeoMetaTags');
        $tag                = $meta_tag_table->newEntity(['seo_uri_id' => $uri_id]);

        if ($this->request->is(['post', 'put'])) {
            $this->request->data['created']     = date('Y-m-d H:i:s');
            $this->request->data['modified']    = date('Y-m-d H:i:s');

            $meta_tag_table->patchEntity($tag, $this->request->data);
            if($meta_tag_table->save($tag)){
                $this->Flash->success('New meta tag has been added.');
                return $this->redirect(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'edit', $uri_id]);
            }
        }

        $this->set('tag', $tag);
    }

    public function edit($tag_id){
        $this->set('page_title', 'URIs SEO Management > URI > Meta Tags > Edit');
        $meta_tag_table     = TableRegistry::get('IconicSeo.SeoMetaTags');
        $tag                = $meta_tag_table->get($tag_id);

        if ($this->request->is(['post', 'put'])) {
            $this->request->data['created']     = date('Y-m-d H:i:s');
            $this->request->data['modified']    = date('Y-m-d H:i:s');

            $meta_tag_table->patchEntity($tag, $this->request->data);
            if($meta_tag_table->save($tag)){
                $this->Flash->success('New meta tag has been updated.');
                return $this->redirect(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'edit', $tag->seo_uri_id]);
            }
        }

        $this->set('tag', $tag);
    }

    public function delete($id){
        $meta_tag_table     = TableRegistry::get('IconicSeo.SeoMetaTags');
        $tag           = $meta_tag_table->get($id);
        $uri_id             = $tag->seo_uri_id;
        if($meta_tag_table->delete($tag)){
            $this->Flash->success('A tag has been deleted.');
        }
        return $this->redirect(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'edit', $uri_id]);
    }

}