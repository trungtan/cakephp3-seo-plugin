<?php
namespace IconicSeo\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Seo\Model\HtmlTag;
use Seo\Model\LinkTag;
use Seo\Model\MetaTag;
use Seo\Model\TextTag;
use Seo\SeoDefault;
use Seo\Model\TagAbstract;
use Seo\SeoManager;

/**
 * Class NewUrisController
 * This is just a quick version to add/edit Seo info
 * This controller and its template will be cloned into Admin Screen
 *
 * @package IconicSeo\Controller
 */
class NewUrisController extends AppController
{

    /**
     * @var SeoManager
     */
    private $_seo_manager;

    public function initialize()
    {
        parent::initialize();
        $this->_seo_manager = new SeoManager();
    }

    //http://iconic-intl.local/iconic_seo/NewUris
    public function index()
    {
        $this->set('page_title', 'Seo management');
        $this->set('seo_bundles', $this->_seo_manager->getAll());
    }

    public function deleteAjax()
    {
        $this->viewBuilder()->layout('ajax');
        if ($this->request->is('post')) {
            $id = $this->request->data['id'];
            if ($this->_seo_manager->deleteById($id)) {
                echo '1';
            } else {
                echo '0';
            }
        }
        $this->autoRender = false;
    }

    public function edit($id)
    {
        $this->set('page_title', 'Edit link | Seo management');
        $tag_bundle = $this->_seo_manager->getOne($id);
        $this->set('tag_bundle', $tag_bundle);

        if ($this->request->is(['post', 'put'])) {
            $tag_bundle->setMetaTags([]);
            $arr_tags = $this->request->data['tags'];

            foreach ((array) $arr_tags as $arr_tag) {
                switch ($arr_tag['type']) {
                    case TagAbstract::TEXT_TAG:
                        $tag = new TextTag();
                        $tag->setTagName($arr_tag['tag_name'])
                            ->setLanguageCode($arr_tag['language_code'] ?? null)
                            ->setInnerText($arr_tag['inner_text'] ?? '');

                        $tag_bundle->addOneTag($tag);
                        break;

                    case TagAbstract::LINK_TAG:
                    case TagAbstract::META_TAG:
                        $tag = new LinkTag();
                        $tag->addManyAttributes($this->convertAttributes($arr_tag['attributes']  ?? []))
                            ->setLanguageCode($arr_tag['language_code'] ?? null);

                        $tag_bundle->addOneTag($tag);
                        break;

                    case TagAbstract::HTML_TAG:
                        $tag = new HtmlTag();
                        $tag->setTagName($arr_tag['tag_name'])
                            ->setInnerText($arr_tag['inner_text'])
                            ->addManyAttributes($this->convertAttributes($arr_tag['attributes']  ?? []))
                            ->setLanguageCode($arr_tag['language_code'] ?? null);

                        $tag_bundle->addOneTag($tag);
                        break;
                }
            }
            $this->_seo_manager->update($id, $tag_bundle);

            //redirect / refresh
        }

        $this->setOptions();
    }

    private function setOptions()
    {
        $this->set(
            'tag_types_options', [
            TagAbstract::META_TAG => 'Meta tag',
            TagAbstract::HTML_TAG => 'Html tag',
            TagAbstract::LINK_TAG => 'Link tag',
            TagAbstract::TEXT_TAG => 'Text tag'
        ]);
        $this->set(
            'language_options',[
            '' => 'All',
            'EN' => "EN",
            'JA' => 'JA',
            'VI' => 'VI'
        ]);
    }

    private function convertAttributes(array $submitted_attributes)
    {
        $result = [];
        foreach ($submitted_attributes as $att) {
            $result[$att['key']] = $att['value'];
        }
        return $result;
    }

}