<?php
/**
 * Created by PhpStorm.
 * User: tanbt
 * Date: 12/06/2015
 * Time: 08:54
 */

namespace IconicSeo\Controller;

use Cake\Event\Event;
use App\Controller\AppController as CommonController;

class AppController extends CommonController{

    public function initialize() {
        parent::initialize();
        $this->layout = 'default';
    }

    public function beforeFilter(Event $event){
        parent::beforeFilter($event);
        $this->Auth->deny();
    }

    public function isAuthorized($user)
    {
        if($user['email'] === ADMIN_EMAIL) {
            return true;
        }
        return false;
    }

}