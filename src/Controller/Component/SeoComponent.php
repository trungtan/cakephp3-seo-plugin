<?php
namespace IconicSeo\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;

class SeoComponent extends Component
{


    protected $_config = [];

    protected $_defaultConfig = [

        'right_part_title' => '|アイコニック - ICONIC Co., Ltd',
        'main_country' => 'マレーシア',
        'year' => '年',
        'month' => '月',

        'pagesOb' => [
            'top_message' => '',
            'footer_keyword' => [
                'ja_keywords' => 'マレーシア,求人,転職,就職,仕事,採用',
                'en_keywords' => 'Malaysia,Job Hunting,Job Change,Find Job,Job,Employment',
                'vi_keywords' => 'Malaysia,Săn tìm việc,Cơ hội nghề nghiệp,Tìm việc,Việc làm,Tuyển dụng',
            ],
            'keywords' => 'マレーシア, 求人, 仕事,就職,人材, 採用, アジア求人, 現地採用, 人材紹介, 転職',
            'description' => 'マレーシア, 求人, マレーシア, 求人, 仕事, 就職, 人材, 採用, アジア, 求人, 現地採用, 人材紹介, 転職'
        ],

        'jobsView' => [
            'page_title' => '{job_title_lo} ({main_country}) {right_part_title}',
            'keywords' => "{main_country} {job_occupation}, {main_country} {job_industry}, {job_code}",
        ],

        'jobsDir' => [
            'type' => '4',
            'page_title' => "マレーシアの求人一覧({filter_by}) {right_part_title}",
            'keywords' => "マレーシア 求人検索 {filter_by}",
            'description' => "{filter_by} の求人一覧です。",
            'filter_by_date' => "{data_year}{year}{data_month}{month}",
            'filter_by_keywords' => "{data_keywords}"
        ],

        'searchLatestJobs' => [
            'type' => 0,
            'page_title' => "最新の求人情報 {right_part_title}",
            'keywords' => "ICONIC お問い合わせ",
            'description' => "マレーシアの最新求人一覧です。お好きな求人を選んでご応募ください。"
        ],

        'userLogIn' => [
            'type' => 0,
            'page_title' => 'ログインページ {right_part_title}',
            'canonical' => 'users/login',
        ],

        'contactOb' => [
            'type' => 0,
            'page_title' => "お問い合わせ {right_part_title}",
            'keywords' => "ICONIC お問い合わせ",
            'description' => "ICONICへの問い合わせフォームです。どんなご質問にもお答えします。お気軽にお問い合わせください。"
        ]

    ];

    /*
     * URI Types (careful when changing the number):
     *  0: auto-generated SEO information
     *      job search, job directory - by title, job directory - by date (same SEO syntax)   (do not store)
     *      pages which require login    (have no SEO information, do not store)
     *      pages for job view detail    (do not store into database)
     *      pages for job search         (do not store into database)
     *      latest job
     *      user login (for redirect)
     *      pages with useless query     (do not store, use the same SEO information with the pages which has no query)
     *
     *  1: (default) other URIs
     *  2: static page from [controller => Pages, action => display]
     *  3: candidate register
     *  4: job directory root-path (jobs/dir)
     *  5: employer pages which have same SEO information with candidate page (store by copying from candidate link)
     *      for example: http://iconic-intl.local/users/login?redirect=%2Fjobs%2Fnhan-vien-phat-trien-kinh-doanh-moi-26535
     *                  will have same SEO information with http://iconic-intl.local/users/login
     *checkUriInfo
     */

    public $_seo_uri_table;


    public function initialize(array $config)
    {
//        var_dump($this->_config);
//        var_dump($this->config('pagesOb')['keywords']);
//        exit;
        $this->_seo_uri_table = TableRegistry::get('IconicSeo.SeoUris');
    }

//    public function __construct($ComponentRegistry, $data = null)
//    {
//        var_dump($this->_config);
//        exit;
//        $this->_seo_uri_table = TableRegistry::get('IconicSeo.SeoUris');
//    }

    public function getPagesDisplayUriObject($param, $menu_title)
    {
        $url = preg_replace('/\?.*/', '', env('REQUEST_URI'));
        $uri_paths = explode('/', $url);
        $uri = "/pages/{$param}";

        if ($uri_paths[1] == "employers") {
            $uri = "/employers/pages/{$param}";
        }

        if ($param == 'home') {
            $uri = '/';
        } elseif ($param == 'faq') {
            $uri = $url;
        } elseif ($param == "employers") {
            $uri = "/employers/";
        }
        //dump($uri);
        $static_page_cache_key = "seoComp_pages_display_param_{$param}";

        $seo_uri = $this->_seo_uri_table->find('all')
            ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals', 'SeoTopMessages', 'SeoFooterKeywords'])
            ->where(['SeoUris.uri = ' => $uri])
            ->first();
//        if (($seo_uri = Cache::read($static_page_cache_key)) === false) {
//            $seo_uri = $this->_seo_uri_table->find('all')
//                ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals','SeoTopMessages'])
//                ->where(['SeoUris.uri = ' => $uri])
//                ->first();
//        }

        if ($seo_uri === NULL) {
            $seo_uri = $this->createNewUriObject([
                'uri' => $uri,
                'type' => '2',
                'top_message' => $this->config('pagesOb')['top_message'],
                'footer_keyword' => $this->config('pagesOb')['footer_keyword'],
                'page_title' => $menu_title,
                'canonical' => PROTOCOL . "://" . $_SERVER['HTTP_HOST'] . $uri,
                'keywords' => $this->config('pagesOb')['keywords'],
                'description' => $this->config('pagesOb')['description']
            ]);
            $this->_seo_uri_table->save($seo_uri);
        } else {
            if (!$seo_uri['top_message']) {
                $this->_seo_uri_table->patchEntity($seo_uri,
                    [
                        'top_message' => [
                            'en_top_message' => '',
                            'vi_top_message' => '',
                            'ja_top_message' => '',
                            'created' => date('Y-m-d H:i:s'),
                            'modified' => date('Y-m-d H:i:s')
                        ],
                    ],
                    [
                        'associated' => ['SeoTopMessages']
                    ]
                );
                $this->_seo_uri_table->save($seo_uri);
            }

            if (!$seo_uri['footer_keyword']) {
                $this->_seo_uri_table->patchEntity($seo_uri,
                    [
                        'footer_keyword' => [
                            'ja_keywords' => $this->config('pagesOb')['footer_keyword']['ja_keywords'],
                            'en_keywords' => $this->config('pagesOb')['footer_keyword']['en_keywords'],
                            'vi_keywords' => $this->config('pagesOb')['footer_keyword']['vi_keywords'],
                            'created' => date('Y-m-d H:i:s'),
                            'modified' => date('Y-m-d H:i:s')
                        ],
                    ],
                    [
                        'associated' => ['SeoFooterKeywords']
                    ]
                );
                $this->_seo_uri_table->save($seo_uri);
            }
        }
//        Cache::write($static_page_cache_key, $seo_uri);
        return $seo_uri;
    }

    private function stringReplaceCommonWord($str)
    {

        $commons = ['right_part_title', 'main_country'];

        foreach ($commons as $word) {

            if (strpos($str, '{' . $word . '}') !== false) {

                $str = str_replace('{' . $word . '}', $this->config($word), $str);
            }
        }

        return $str;
    }

    private function stringReplaceDefined($hold, $str)
    {

        foreach ($hold as $word => $value) {
            if (strpos($str, '{' . $word . '}') !== false) {
                $str = str_replace('{' . $word . '}', $value, $str);
            }
        }

        return $str;
    }

    public function getJobsViewUriObject($job)
    {
        function stringReplaceJobView($str, $job_prop, $job)
        {

            foreach ($job_prop as $prop) {

                if (strpos($str, '{' . $prop . '}') !== false) {
                    if (is_string($job[substr($prop, 4)]))
                        $str = str_replace('{' . $prop . '}', $job[substr($prop, 4)], $str);
                }
            }
            return $str;
        }

        $job_prop_replace = ['job_title_lo', 'job_occupation', 'job_industry', 'job_code'];

        $page_title = $this->stringReplaceCommonWord($this->config('jobsView')['page_title']);
        $meta_keyword = $this->stringReplaceCommonWord($this->config('jobsView')['keywords']);

        $page_title = mb_substr(stringReplaceJobView($page_title, $job_prop_replace, $job), 0, 75);
        $meta_keyword = stringReplaceJobView($meta_keyword, $job_prop_replace, $job);


        $job_meta_description = $job->title_lo . ' - ' . preg_replace('/\s+/', ' ', str_replace(array("\n", "\r", '  ', '&nbsp;'), ' ', strip_tags(str_replace('<', ' <', $job->job_description))));
        $job_meta_description = mb_substr($job_meta_description, 0, 150);


        $seo_uri = $this->createNewUriObject([
            'uri' => '',
            'type' => 0,
            'page_title' => $page_title,
            'canonical' => PROTOCOL . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"],
            'keywords' => $meta_keyword,
            'description' => $job_meta_description,
            'top_message' => ''
        ]);

        return $seo_uri;
    }


    /*
     * controller => 'Jobs', action=>'job_directory'
     */
    public function getJobsDirectoryUriObject($data = [])
    {

        $filter_by = '';

        $dir_replace = [
            'year' => $this->config('year'),
            'month' => $this->config('month'),
            'data_year' => isset($data['year']) ? $data['year'] : '',
            'data_month' => isset($data['month']) ? $data['month'] : '',
            'data_keywords' => isset($data['keywords']) ? $data['keywords'] : '',
        ];

        if (isset($data['keywords'])) {
            $filter_by = $this->config('jobsDir')['filter_by_keywords'];
        } elseif (isset($data['year']) && isset($data['month'])) {
            $filter_by = $this->config('jobsDir')['filter_by_date'];
        }

        $filter_by = $this->stringReplaceDefined($dir_replace, $filter_by);


        if (empty($filter_by)) {
            $seo_uri = $this->_seo_uri_table->find('all')
                ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'])
                ->where(['SeoUris.uri = ' => '/jobs/dir'])
                ->first();
        } else {


            $hold = ['filter_by' => $filter_by];

            $page_title = $this->stringReplaceCommonWord($this->config('jobsDir')['page_title']);
            $keywords = $this->stringReplaceCommonWord($this->config('jobsDir')['keywords']);
            $description = $this->stringReplaceCommonWord($this->config('jobsDir')['description']);

            $page_title = $this->stringReplaceDefined($hold, $page_title);
            $keywords = $this->stringReplaceDefined($hold, $keywords);
            $description = $this->stringReplaceDefined($hold, $description);


            $seo_uri = $this->createNewUriObject([
                'uri' => '',
                'type' => $this->config('jobsDir')['type'],
                'page_title' => $page_title,
                //'canonical'   => strtok(PROTOCOL . "://" . $_SERVER["HTTP_HOST"] . str_replace('/employers', '', $_SERVER["REQUEST_URI"]), '?'),
                'canonical' => PROTOCOL . "://" . $_SERVER["HTTP_HOST"] . str_replace('/employers', '', $_SERVER["REQUEST_URI"]),
                'keywords' => $keywords,
                'description' => $description,
            ]);
        }
        return $seo_uri;
    }

    public function getSearchJobUriObject($keyword)
    {
        return $this->getJobsDirectoryUriObject(['keywords' => $keyword]);
    }

    public function getSearchLatestJobUriObject()
    {
        $seo_uri = $this->createNewUriObject([
            'uri' => '',
            'type' => $this->config('searchLatestJobs')['type'],
            'page_title' => $this->stringReplaceCommonWord($this->config('searchLatestJobs')['page_title']),
            'canonical' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
            'keywords' => $this->stringReplaceCommonWord($this->config('searchLatestJobs')['keywords']),
            'description' => $this->stringReplaceCommonWord($this->config('searchLatestJobs')['description']),
        ]);

        return $seo_uri;
    }

    public function getUserLogInUriObject()
    {
        $new_uri = $this->_seo_uri_table->newEntity([
            'type' => $this->config('userLogIn')['type'],
            'title' => [
                'title' => $this->stringReplaceCommonWord($this->config('userLogIn')['page_title']),
            ],
            'canonical' => [
                'canonical' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/" . $this->config('userLogIn')['canonical'],
            ],
            'meta_tags' => [
                [
                    'attribute1_title' => 'name',
                    'attribute1_content' => 'robots',
                    'attribute2_title' => 'content',
                    'attribute2_content' => 'noindex, nofollow'
                ],
            ]
        ],
            ['associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals']]
        );
        return $new_uri;
    }

    public function getContactUriObject()
    {

        $seo_uri = $this->createNewUriObject([
            'uri' => '',
            'type' => $this->config('contactOb')['type'],
            'page_title' => $this->stringReplaceCommonWord($this->config('contactOb')['page_title']),
            'canonical' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
            'keywords' => $this->stringReplaceCommonWord($this->config('contactOb')['keywords']),
            'description' => $this->stringReplaceCommonWord($this->config('contactOb')['description']),
        ]);

        return $seo_uri;

    }

    public function getCandidatesRegisterUriObject()
    {
        $uri = '/candidates/register';
        $candidate_register_cache_key = "seoComp_candidate_register_uri_obj";

        if (($seo_uri = Cache::read($candidate_register_cache_key)) === false) {
            $seo_uri = $this->_seo_uri_table->find('all')
                ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'])
                ->where(['SeoUris.uri = ' => $uri])
                ->first();

            if ($seo_uri == NULL) {
                $seo_uri = $this->createNewUriObject([
                    'uri' => $uri,
                    'type' => 3,
                    'page_title' => "メンバー登録 |アイコニック - ICONIC Co., Ltd",
                    'canonical' => PROTOCOL . "://" . $_SERVER['HTTP_HOST'] . $uri,
                    'keywords' => "マレーシア アカウント登録,ICONIC アカウント登録,アイコニック アカウント登録",
                    'description' => "アカウント登録並びに、確認メールの処理を行っていただけると、ICONICの求人情報にアクセスすることができます。引き続いて、転職支援サービスの登録もご利用になれます。"
                ]);
                $this->_seo_uri_table->save($seo_uri);
            }
            Cache::write($candidate_register_cache_key, $seo_uri);
        }

        return $seo_uri;
    }

    public function getAuthorizedPagesUriObject($data = [])
    {
        $seo_uri = $this->_seo_uri_table->newEntity([
            'uri' => '',
            'type' => 0,
            'title' => [
                'title' => $data['page_title'],
            ],
            'canonical' => [
                'canonical' => PROTOCOL . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"],
            ],
            'meta_tags' => [
                [
                    'attribute1_title' => 'name',
                    'attribute1_content' => 'robots',
                    'attribute2_title' => 'content',
                    'attribute2_content' => 'noindex, nofollow'
                ],
            ]
        ],
            ['associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals']]
        );

        return $seo_uri;
    }

    public function getCandidatesUnsubscribeUriObject($title)
    {
        $new_uri = $this->_seo_uri_table->newEntity([
            'type' => 0,
            'title' => [
                'title' => "{$title} |アイコニック - ICONIC Co., Ltd",
            ],
            'canonical' => [
                'canonical' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/candidates/unsubscribe",
            ],
            'meta_tags' => [
                [
                    'attribute1_title' => 'name',
                    'attribute1_content' => 'robots',
                    'attribute2_title' => 'content',
                    'attribute2_content' => 'noindex, nofollow'
                ],
            ]
        ],
            ['associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals']]
        );
        return $new_uri;
    }

    public function getEmployersUnsubscribeUriObject($title)
    {
        $new_uri = $this->_seo_uri_table->newEntity([
            'type' => 0,
            'title' => [
                'title' => "{$title} |アイコニック - ICONIC Co., Ltd",
            ],
            'canonical' => [
                'canonical' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/employers/unsubscribe",
            ],
            'meta_tags' => [
                [
                    'attribute1_title' => 'name',
                    'attribute1_content' => 'robots',
                    'attribute2_title' => 'content',
                    'attribute2_content' => 'noindex, nofollow'
                ],
            ]
        ],
            ['associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals']]
        );
        return $new_uri;
    }

    private function createNewUriObject($data = [])
    {
        $meta_tags =
            [
                [
                    'attribute1_title' => 'name',
                    'attribute1_content' => 'keywords',
                    'attribute2_title' => 'content',
                    'attribute2_content' => $data['keywords']
                ],
                [
                    'attribute1_title' => 'name',
                    'attribute1_content' => 'description',
                    'attribute2_title' => 'content',
                    'attribute2_content' => $data['description']
                ],
                [
                    'attribute1_title' => 'name',
                    'attribute1_content' => 'robots',
                    'attribute2_title' => 'content',
                    'attribute2_content' => 'index, follow'
                ],
                [
                    'attribute1_title' => 'property',
                    'attribute1_content' => 'og:title',
                    'attribute2_title' => 'content',
                    'attribute2_content' => $data['page_title']
                ],
                [
                    'attribute1_title' => 'property',
                    'attribute1_content' => 'og:description',
                    'attribute2_title' => 'content',
                    'attribute2_content' => isset($data['og_description']) ? $data['og_description'] : $data['description']
                ],
                [
                    'attribute1_title' => 'property',
                    'attribute1_content' => 'og:url',
                    'attribute2_title' => 'content',
                    'attribute2_content' => $data['canonical']
                ],
                [
                    'attribute1_title' => 'property',
                    'attribute1_content' => 'og:image',
                    'attribute2_title' => 'content',
                    'attribute2_content' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/" . (isset($data['og_image'])
                            ? $data['og_image'] : 'img/ogp.png')
                ]
            ];

        if (isset($data['additional_meta'])) {
            $meta_tags = array_merge($meta_tags, $data['additional_meta']);
        }

        $new_uri = $this->_seo_uri_table->newEntity([
            'uri' => $data['uri'],
            'type' => $data['type'],
            'created' => date('Y-m-d H:i:s'),
            'modified' => date('Y-m-d H:i:s'),
            'top_message' => [
                'top_message' => (isset($data['top_message'])) ? $data['top_message'] : "",
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            'title' => [
                'title' => $data['page_title'],
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            'canonical' => [
                'canonical' => $data['canonical'],
                'is_active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            ],
            'meta_tags' => $meta_tags,
        ],
            ['associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals']]
        );
        return $new_uri;
    }

}