<?php
namespace IconicSeo\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;

class SeoComponent extends Component {

    /*
     * URI Types (careful when changing the number):
     *  0: auto-generated SEO information
     *      job search, job directory - by title, job directory - by date (same SEO syntax)   (do not store)
     *      pages which require login    (have no SEO information, do not store)
     *      pages for job view detail    (do not store into database)
     *      pages for job search         (do not store into database)
     *      latest job
     *      user login (for redirect)
     *      pages with useless query     (do not store, use the same SEO information with the pages which has no query)
     *
     *  1: (default) other URIs
     *  2: static page from [controller => Pages, action => display]
     *  3: candidate register
     *  4: job directory root-path (jobs/dir)
     *  5: employer pages which have same SEO information with candidate page (store by copying from candidate link)
     *      for example: http://iconic-intl.local/users/login?redirect=%2Fjobs%2Fnhan-vien-phat-trien-kinh-doanh-moi-26535
     *                  will have same SEO information with http://iconic-intl.local/users/login
     *checkUriInfo
     */

    public $_seo_uri_table;

    public function __construct($ComponentRegistry, $data = null)
    {
        $this->_seo_uri_table = TableRegistry::get('IconicSeo.SeoUris');
    }

    public function getPagesDisplayUriObject($param, $menu_title){
        $url = preg_replace('/\?.*/', '', env('REQUEST_URI'));
        $uri_paths = explode('/', $url);
        $uri = "/pages/{$param}";

        if($uri_paths[1]=="employers"){
            $uri = "/employers/pages/{$param}";
        }

        if($param == 'home') {
            $uri = '/';
        }
        elseif ($param == 'faq') {
            $uri = $url;
        }
        elseif($param=="employers"){
            $uri = "/employers/";
        }
        //dump($uri);
        $static_page_cache_key = "seoComp_pages_display_param_{$param}";

        $seo_uri = $this->_seo_uri_table->find('all')
            ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals','SeoTopMessages','SeoFooterKeywords'])
            ->where(['SeoUris.uri = ' => $uri])
            ->first();
//        if (($seo_uri = Cache::read($static_page_cache_key)) === false) {
//            $seo_uri = $this->_seo_uri_table->find('all')
//                ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals','SeoTopMessages'])
//                ->where(['SeoUris.uri = ' => $uri])
//                ->first();
//        }

        if($seo_uri === NULL){
            $seo_uri = $this->createNewUriObject([
                'uri'           => $uri,
                'type'          => 2,
                'top_message'   => '',
                'footer_keyword'   => [
                    'ja_keywords' => 'マレーシア,求人,転職,就職,仕事,採用',
                    'en_keywords' => 'Malaysia,Job Hunting,Job Change,Find Job,Job,Employment',
                    'vi_keywords' => 'Malaysia,Săn tìm việc,Cơ hội nghề nghiệp,Tìm việc,Việc làm,Tuyển dụng',
                ],
                'page_title'    => $menu_title,
                'canonical'     => PROTOCOL . "://" . $_SERVER['HTTP_HOST'] . $uri,
                'keywords'       => 'マレーシア, 求人, 仕事,就職,人材, 採用, アジア求人, 現地採用, 人材紹介, 転職',
                'description'   => 'マレーシア, 求人, マレーシア, 求人, 仕事, 就職, 人材, 採用, アジア, 求人, 現地採用, 人材紹介, 転職'
            ]);
            $this->_seo_uri_table->save($seo_uri);
        }
        else{
            if(!$seo_uri['top_message']){
                $this->_seo_uri_table->patchEntity($seo_uri,
                    [
                        'top_message'   => [
                            'en_top_message' => '',
                            'vi_top_message' => '',
                            'ja_top_message' => '',
                            'created'       => date('Y-m-d H:i:s'),
                            'modified'      => date('Y-m-d H:i:s')
                        ],
                    ],
                    [
                        'associated' => ['SeoTopMessages']
                    ]
                );
                $this->_seo_uri_table->save($seo_uri);
            }

            if(!$seo_uri['footer_keyword']){
                $this->_seo_uri_table->patchEntity($seo_uri,
                    [
                        'footer_keyword'   => [
                            'ja_keywords' => 'マレーシア,求人,転職,就職,仕事,採用',
                            'en_keywords' => 'Malaysia,Job Hunting,Job Change,Find Job,Job,Employment',
                            'vi_keywords' => 'Malaysia,Săn tìm việc,Cơ hội nghề nghiệp,Tìm việc,Việc làm,Tuyển dụng',
                            'created'       => date('Y-m-d H:i:s'),
                            'modified'      => date('Y-m-d H:i:s')
                        ],
                    ],
                    [
                        'associated' => ['SeoFooterKeywords']
                    ]
                );
                $this->_seo_uri_table->save($seo_uri);
            }
        }
//        Cache::write($static_page_cache_key, $seo_uri);
        return $seo_uri;
    }

    public function getJobsViewUriObject($job){
        $job_meta_description = $job->title_lo . ' - ' .  preg_replace('/\s+/', ' ', str_replace( array("\n", "\r", '  ', '&nbsp;'), ' ', strip_tags(str_replace( '<', ' <', $job->job_description))));
        $job_meta_description = mb_substr($job_meta_description, 0, 150);

        $page_title = mb_substr($job->title_lo . ' (マレーシア) |アイコニック - ICONIC Co., Ltd', 0, 75);
        $meta_keyword =  "マレーシア {$job->occupation},マレーシア {$job->industry},{$job->code}";

        $seo_uri = $this->createNewUriObject([
            'uri'           => '',
            'type'          => 0,
            'page_title'    => $page_title,
            'canonical'     => PROTOCOL . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"],
            'keywords'       => $meta_keyword,
            'description'   => $job_meta_description,
            'top_message' =>''
        ]);

        return $seo_uri;
    }

    /*
     * controller => 'Jobs', action=>'job_directory'
     */
    public function getJobsDirectoryUriObject($data = []){

        if(isset($data['keywords'])) {
            $filter_by = $data['keywords'];
        } elseif(isset($data['year']) && isset($data['month'])) {
            $filter_by = "{$data['year']}年{$data['month']}月";
        }

        if(empty($filter_by)){
            $seo_uri = $this->_seo_uri_table->find('all')
                ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'])
                ->where(['SeoUris.uri = ' => '/jobs/dir'])
                ->first();
        } else {
            $seo_uri = $this->createNewUriObject([
                'uri'           => '',
                'type'          => 4,
                'page_title'    => "マレーシアの求人一覧({$filter_by}) |アイコニック - ICONIC Co., Ltd",
                //'canonical'   => strtok(PROTOCOL . "://" . $_SERVER["HTTP_HOST"] . str_replace('/employers', '', $_SERVER["REQUEST_URI"]), '?'),
                'canonical'     => PROTOCOL . "://" . $_SERVER["HTTP_HOST"] . str_replace('/employers', '', $_SERVER["REQUEST_URI"]),
                'keywords'      => "マレーシア 求人検索 {$filter_by}",
                'description'   => "{$filter_by} の求人一覧です。"
            ]);
        }
        return $seo_uri;
    }

    public function getSearchJobUriObject($keyword){
        return $this->getJobsDirectoryUriObject(['keywords' => $keyword]);
    }

    public function getSearchLatestJobUriObject() {
        $seo_uri = $this->createNewUriObject([
            'uri'           => '',
            'type'          => 0,
            'page_title'    => "最新の求人情報 |アイコニック - ICONIC Co., Ltd",
            'canonical'     => PROTOCOL . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}",
            'keywords'       => "マレーシア 最新 求人 一覧",
            'description'   => "マレーシアの最新求人一覧です。お好きな求人を選んでご応募ください。"
        ]);

        return $seo_uri;
    }

    public function getUserLogInUriObject(){
        $new_uri = $this->_seo_uri_table->newEntity([
            'type'          => 0,
            'title'         => [
                'title'     => "ログインページ |アイコニック - ICONIC Co., Ltd",
            ],
            'canonical'     => [
                'canonical' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/users/login",
            ],
            'meta_tags'     => [
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'robots',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'noindex, nofollow'
                ],
            ]
        ],
            [ 'associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'] ]
        );
        return $new_uri;
    }

    public function getContactUriObject(){
        $new_uri = $this->_seo_uri_table->newEntity([
            'type'          => 0,
            'title'         => [
                'title'     => "お問い合わせ |アイコニック - ICONIC Co., Ltd",
            ],
            'canonical'     => [
                'canonical' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/contacts",
            ],
            'meta_tags'     => [
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'keywords',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'ICONIC お問い合わせ'
                ],
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'description',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'ICONICへの問い合わせフォームです。どんなご質問にもお答えします。お気軽にお問い合わせください。'
                ],
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'robots',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'index, follow'
                ],
                [
                    'attribute1_title'      => 'property',
                    'attribute1_content'    => 'og:title',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'お問い合わせ |アイコニック - ICONIC Co., Ltd'
                ],
                [
                    'attribute1_title'      => 'property',
                    'attribute1_content'    => 'og:description',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'ICONICへの問い合わせフォームです。どんなご質問にもお答えします。お気軽にお問い合わせください。'
                ],
                [
                    'attribute1_title'      => 'property',
                    'attribute1_content'    => 'og:url',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/contacts",
                ],
                [
                    'attribute1_title'      => 'property',
                    'attribute1_content'    => 'og:image',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/img/ogp.png",
                ]
            ]
        ],
            [ 'associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'] ]
        );
        return $new_uri;
    }

    public function getCandidatesRegisterUriObject(){
        $uri = '/candidates/register';
        $candidate_register_cache_key = "seoComp_candidate_register_uri_obj";

        if (($seo_uri = Cache::read($candidate_register_cache_key)) === false) {
            $seo_uri = $this->_seo_uri_table->find('all')
                ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'])
                ->where(['SeoUris.uri = ' => $uri])
                ->first();

            if($seo_uri == NULL) {
                $seo_uri = $this->createNewUriObject([
                    'uri'           => $uri,
                    'type'          => 3,
                    'page_title'    => "メンバー登録 |アイコニック - ICONIC Co., Ltd",
                    'canonical'     => PROTOCOL . "://" . $_SERVER['HTTP_HOST'] . $uri,
                    'keywords'       => "マレーシア アカウント登録,ICONIC アカウント登録,アイコニック アカウント登録",
                    'description'   => "アカウント登録並びに、確認メールの処理を行っていただけると、ICONICの求人情報にアクセスすることができます。引き続いて、転職支援サービスの登録もご利用になれます。"
                ]);
                $this->_seo_uri_table->save($seo_uri);
            }
            Cache::write($candidate_register_cache_key, $seo_uri);
        }

        return $seo_uri;
    }

    public function getAuthorizedPagesUriObject ($data = []){
        $seo_uri = $this->_seo_uri_table->newEntity([
            'uri'           => '',
            'type'          => 0,
            'title'         => [
                'title'     => $data['page_title'],
            ],
            'canonical'     => [
                'canonical' => PROTOCOL . "://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"],
            ],
            'meta_tags'     => [
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'robots',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'noindex, nofollow'
                ],
            ]
        ],
            [ 'associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'] ]
        );

        return $seo_uri;
    }

    public function getCandidatesUnsubscribeUriObject($title){
        $new_uri = $this->_seo_uri_table->newEntity([
            'type'          => 0,
            'title'         => [
                'title'     => "{$title} |アイコニック - ICONIC Co., Ltd",
            ],
            'canonical'     => [
                'canonical' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/candidates/unsubscribe",
            ],
            'meta_tags'     => [
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'robots',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'noindex, nofollow'
                ],
            ]
        ],
            [ 'associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'] ]
        );
        return $new_uri;
    }
    public function getEmployersUnsubscribeUriObject($title){
        $new_uri = $this->_seo_uri_table->newEntity([
            'type'          => 0,
            'title'         => [
                'title'     => "{$title} |アイコニック - ICONIC Co., Ltd",
            ],
            'canonical'     => [
                'canonical' => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/employers/unsubscribe",
            ],
            'meta_tags'     => [
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'robots',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'noindex, nofollow'
                ],
            ]
        ],
            [ 'associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'] ]
        );
        return $new_uri;
    }

    private function createNewUriObject($data = []) {
        $new_uri = $this->_seo_uri_table->newEntity([
            'uri'           => $data['uri'],
            'type'          => $data['type'],
            'created'       => date('Y-m-d H:i:s'),
            'modified'      => date('Y-m-d H:i:s'),
            'top_message'   => [
                'top_message' =>(isset($data['top_message']))? $data['top_message']: "",
                'created'       => date('Y-m-d H:i:s'),
                'modified'      => date('Y-m-d H:i:s')
            ],
            'title'         => [
                'title'     => $data['page_title'],
                'created'       => date('Y-m-d H:i:s'),
                'modified'      => date('Y-m-d H:i:s')
            ],
            'canonical'     => [
                'canonical' => $data['canonical'],
                'is_active' => 1,
                'created'       => date('Y-m-d H:i:s'),
                'modified'      => date('Y-m-d H:i:s')
            ],
            'meta_tags'     => [
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'keywords',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => $data['keywords']
                ],
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'description',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => $data['description']
                ],
                [
                    'attribute1_title'      => 'name',
                    'attribute1_content'    => 'robots',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => 'index, follow'
                ],
                [
                    'attribute1_title'      => 'property',
                    'attribute1_content'    => 'og:title',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => $data['page_title']
                ],
                [
                    'attribute1_title'      => 'property',
                    'attribute1_content'    => 'og:description',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => $data['description']
                ],
                [
                    'attribute1_title'      => 'property',
                    'attribute1_content'    => 'og:url',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => $data['canonical']
                ],
                [
                    'attribute1_title'      => 'property',
                    'attribute1_content'    => 'og:image',
                    'attribute2_title'      => 'content',
                    'attribute2_content'    => PROTOCOL . "://{$_SERVER['HTTP_HOST']}/img/ogp.png"
                ]
            ]
        ],
            [ 'associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'] ]
        );
        return $new_uri;
    }

}