<?php
/**
 * Created by PhpStorm.
 * User: tanbt
 * Date: 12/06/2015
 * Time: 13:46
 */

namespace IconicSeo\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class RedirectsController extends AppController{


    public function add($uri_id){
        $this->set('page_title', 'URIs SEO Management > URI > Redirect > Add');
        $redirect_table     = TableRegistry::get('IconicSeo.SeoRedirects');
        $redirect           = $redirect_table->newEntity(['seo_uri_id' => $uri_id]);

        if ($this->request->is(['post', 'put'])) {
            $this->request->data['created']     = date('Y-m-d H:i:s');
            $this->request->data['modified']    = date('Y-m-d H:i:s');

            $redirect_table->patchEntity($redirect, $this->request->data);
            if($redirect_table->save($redirect)){
                $this->Flash->success('New redirect has been added.');
                return $this->redirect(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'edit', $uri_id]);
            }
        }

        $this->set('redirect', $redirect);
    }

    public function delete($id){
        $redirect_table     = TableRegistry::get('IconicSeo.SeoRedirects');
        $redirect           = $redirect_table->get($id);
        $uri_id             = $redirect->seo_uri_id;
        if($redirect_table->delete($redirect)){
            $this->Flash->success('Redirect link has been deleted.');
        }
        return $this->redirect(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'edit', $uri_id]);
    }

}