<?php
namespace IconicSeo\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Seo\Model\TextTag;
use Seo\SeoDefault;

class UrisController extends AppController
{

    //http://iconic-intl.local/iconic_seo/uris
    public function index()
    {
        $filter = $this->request->query('filter') ? $this->request->query('filter') : 'all';
        $orderBy = $this->request->query('order') ? $this->request->query('order') : '';

        $this->set('page_title', 'URIs SEO Management');

        $seo_uris = TableRegistry::get('IconicSeo.SeoUris');
        $query = $seo_uris->find('all')
            ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals', 'SeoRedirects', 'SeoTopMessages', 'SeoFooterKeywords']);

        switch ($filter) {
            case 'jobs_dir':
                $query->where(['SeoUris.type' => 4]);
                break;
            case 'static_pages':
                $query->where(['SeoUris.type' => 2]);
                break;
            case 'other':
                $query->where(['SeoUris.type NOT IN' => [2, 4]]);
                break;
            default:
                break;
        }

        switch ($orderBy) {
            case 'created':
                $query->order(['SeoUris.created' => 'DESC']);
                break;
            case 'modified':
                $query->order(['SeoUris.modified' => 'DESC']);
                break;
            default:
                break;
        }

        if ($this->request->query('kw')) {
            $kw = $this->request->query('kw');
            $query->where(['SeoUris.uri LIKE ' => "%{$kw}%"]);
            $this->set('kw', $kw);
        }

        $this->paginate = [
            'limit' => 20
        ];
        $this->set('all_uris', $this->paginate($query));
    }

    public function edit($id)
    {
        $this->set('page_title', 'URIs SEO Management > URI');

        $seo_uris = TableRegistry::get('IconicSeo.SeoUris');
        $uri = $seo_uris->get($id, [
            'contain' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals', 'SeoRedirects', 'SeoTopMessages', 'SeoFooterKeywords']
        ]);

        if ($this->request->is(['post', 'put'])) {

            $seo_uris->patchEntity($uri, $this->request->data, [
                'associated' => ['SeoTitles', 'SeoCanonicals', 'SeoRedirects', 'SeoTopMessages', 'SeoFooterKeyWords']
            ]);

            if ($uri->dirty('title')) {
                $this->request->data['modified'] = date('Y-m-d H:i:s');
                $this->request->data['title']['modified'] = date('Y-m-d H:i:s');
            }
            if ($uri->dirty('canonical')) {
                $this->request->data['modified'] = date('Y-m-d H:i:s');
                $this->request->data['canonical']['modified'] = date('Y-m-d H:i:s');
            }
            if ($uri->dirty('en_top_message')) {
                $this->request->data['modified'] = date('Y-m-d H:i:s');
                $this->request->data['en_top_message']['modified'] = date('Y-m-d H:i:s');
            }
            if ($uri->dirty('ja_top_message')) {
                $this->request->data['modified'] = date('Y-m-d H:i:s');
                $this->request->data['ja_top_message']['modified'] = date('Y-m-d H:i:s');
            }
            if ($uri->dirty('vi_top_message')) {
                $this->request->data['modified'] = date('Y-m-d H:i:s');
                $this->request->data['vi_top_message']['modified'] = date('Y-m-d H:i:s');
            }
            if ($uri->dirty('en_keywords')) {
                $this->request->data['modified'] = date('Y-m-d H:i:s');
                $this->request->data['en_keywords']['modified'] = date('Y-m-d H:i:s');
            }
            if ($uri->dirty('ja_keyword')) {
                $this->request->data['modified'] = date('Y-m-d H:i:s');
                $this->request->data['ja_keywords']['modified'] = date('Y-m-d H:i:s');
            }
            if ($uri->dirty('vi_keyword')) {
                $this->request->data['modified'] = date('Y-m-d H:i:s');
                $this->request->data['vi_keywords']['modified'] = date('Y-m-d H:i:s');
            }
            if ($uri->dirty('redirect')) {
                $this->request->data['modified'] = date('Y-m-d H:i:s');
                $this->request->data['redirect']['modified'] = date('Y-m-d H:i:s');
            }

            $seo_uris->patchEntity($uri, $this->request->data, [
                'associated' => ['SeoTitles', 'SeoCanonicals', 'SeoRedirects', 'SeoFooterKeywords', 'SeoTopMessages']
            ]);
            if ($seo_uris->save($uri)) {
                $this->Flash->success(__('URI has been updated.'));
            }
        }

        $this->set('uri', $uri);
    }

    /*
    public function changeStatus(){
        $uri_id = $this->request->query('id');
        $page   = $this->request->query('page');

        $seo_uris = TableRegistry::get('IconicSeo.SeoUris');
        $uri = $seo_uris->get($uri_id);
        $uri->is_approved = ($uri->is_approved + 1) % 2;
        $uri->modified = date('Y-m-d H:i:s');
        if($seo_uris->save($uri)){
            $this->Flash->success("Uri '{$uri_id}' has been updated.");
        }

        return $this->redirect(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index',
            '#' => "uri_{$uri_id}",
            '?' => ['page' => $page],
        ]);
    }
    */

    public function deleteUri()
    {
        $uri_id = $this->request->query('id');
        $page = $this->request->query('page');

        $seo_uris = TableRegistry::get('IconicSeo.SeoUris');
        $uri = $seo_uris->get($uri_id);
        if ($seo_uris->delete($uri)) {
            TableRegistry::get('IconicSeo.SeoTitles')->deleteAll(['seo_uri_id' => $uri_id]);
            TableRegistry::get('IconicSeo.SeoCanonicals')->deleteAll(['seo_uri_id' => $uri_id]);
            TableRegistry::get('IconicSeo.SeoMetaTags')->deleteAll(['seo_uri_id' => $uri_id]);
            TableRegistry::get('IconicSeo.SeoRedirects')->deleteAll(['seo_uri_id' => $uri_id]);

            $this->Flash->success("Uri '{$uri_id}' has been deleted.");
        }

        return $this->redirect(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index',
            '#' => "uri_{$uri_id}",
            '?' => ['page' => $page],
        ]);
    }

    /**
     * Don't convert redirect link because it can cause unlimited loop
     * <meta http-equiv="refresh" content="0; url=http://example.com/" />
     */
    public function convert()
    {
        echo "Converting...<br/>";

        $seo_uris = TableRegistry::get('IconicSeo.SeoUris');
        $all_uris = $seo_uris->find('all')
            ->contain(['SeoTitles', 'SeoMetaTags', 'SeoCanonicals', 'SeoRedirects', 'SeoTopMessages', 'SeoFooterKeywords'])
            ->order(['uri' => 'ASC'])
            ->toArray();

        foreach ($all_uris as $uri_obj) {
            $full_uri = rtrim(PROTOCOL . "://" . $_SERVER["HTTP_HOST"] . $uri_obj->uri, '/');
            $seo_new_obj = new SeoDefault($full_uri);
            $seo_new_obj
                /*->addMetaTag(
                    [
                        'http-equiv'      => 'refresh',
                        'content'         => "0; url={$uri_obj->redirect}"
                    ]
                )*/
                ->addHtmlTag('title', $uri_obj->title->title)
                ->addLinkTag(
                    ['rel'=>'canonical', 'href' => $uri_obj->canonical->canonical]
                )

                ->addTextTag(TextTag::FOOTER_KEYWORDS, $uri_obj->footer_keyword->ja_keywords ?? '', 'JA')
                ->addTextTag(TextTag::FOOTER_KEYWORDS, $uri_obj->footer_keyword->en_keywords ?? '', 'EN')
                ->addTextTag(TextTag::FOOTER_KEYWORDS, $uri_obj->footer_keyword->vi_keywords ?? '', 'VI')

                ->addTextTag(TextTag::TOP_MESSAGE, $uri_obj->top_message->ja_top_message ?? '', 'JA')
                ->addTextTag(TextTag::TOP_MESSAGE, $uri_obj->top_message->en_top_message ?? '', 'EN')
                ->addTextTag(TextTag::TOP_MESSAGE, $uri_obj->top_message->vi_top_message ?? '', 'VI');

            foreach ($uri_obj->meta_tags as $meta_tag) {
                $seo_new_obj->addMetaTag(
                    [
                        $meta_tag->attribute1_title => $meta_tag->attribute1_content,
                        $meta_tag->attribute2_title => $meta_tag->attribute2_content
                    ]
                );
            }
            $seo_new_obj->save();
            echo "Done: {$full_uri}<br/>";
        }
        echo 'FINISH!!!';
        return $this->redirect(['plugin' => 'IconicSeo', 'controller' => 'Uris', 'action' => 'index']);
        $this->autoRender = false;
    }
}