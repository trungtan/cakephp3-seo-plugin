<?php
/**
 * Created by PhpStorm.
 * User: tanbt
 * Date: 05/06/2015
 * Time: 10:46
 */

namespace IconicSeo\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;

class SeoHelper extends Helper {

    public $helpers = ['Text'];

    public function title($seo_uri = NULL)
    {
        $title_content = 'マレーシアで求人-転職-就職-採用 |アイコニック - ICONIC Co., Ltd';
        if(empty($seo_uri->title)){
            $seo_uri = $this->getUriInfo(env('REQUEST_URI'), ['SeoTitles']);
        }

        if($seo_uri !== NULL && $seo_uri->title !== NULL) {
            $title_content = $seo_uri->title->title;
        }
        return '<title>' . $title_content . '</title>';
    }
    public function top_message($seo_uri = NULL,$locale)
    {
        $uri =  preg_replace('/\?.*/', '', env('REQUEST_URI'));
//        $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
//        if(strtolower($segments[0])=="employers"){
//            $uri="";
//            for($i=1; $i<count($segments);$i++){
//                $uri=$uri."/".$segments[$i];
//            }
//        }
        $top_message="";
        if(empty($seo_uri->top_message)){
            $seo_uri = $this->getUriInfo($uri, ['SeoTopMessages']);
        }

        if($seo_uri !== NULL && $seo_uri->top_message !== NULL) {
            if(isset($seo_uri->top_message[$locale.'_top_message']))
                $top_message = $seo_uri->top_message[$locale.'_top_message'];
        }

        return $top_message;
    }
    public function footer_keywords($seo_uri = NULL,$locale)
    {
        $uri =  preg_replace('/\?.*/', '', env('REQUEST_URI'));
//        $segments = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'));
//        if(strtolower($segments[0])=="employers"){
//            $uri="";
//            for($i=1; $i<count($segments);$i++){
//                $uri=$uri."/".$segments[$i];
//            }
//        }

        $footer_keywords="";
        if(empty($seo_uri->footer_keyword)){
            $seo_uri = $this->getUriInfo($uri, ['SeoFooterKeywords']);
        }

        if($seo_uri !== NULL && $seo_uri->footer_keyword !== NULL) {
            $footer_keywords = $seo_uri->footer_keyword[$locale.'_keywords'];
        }
        //dump($footer_keywords); exit;
        return $footer_keywords;
    }

    public function canonical($seo_uri = NULL)
    {
        if(empty($seo_uri->canonical)){
            $seo_uri = $this->getUriInfo(env('REQUEST_URI'), ['SeoCanonicals'], ['SeoCanonicals.is_active = ' => 1]);
        }

        if($seo_uri !== NULL && $seo_uri->canonical !== NULL) {
            $canonical_link = $seo_uri->canonical->canonical;
        } else {
            $canonical_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        }
        return '<link rel="canonical" href="' . $canonical_link . '">';
    }

    //delete  FROM `seo_uri` WHERE `uri` LIKE '/users/login%'
    public function meta_tags($seo_uri = NULL){
        if(empty($seo_uri->meta_tags)){
            $seo_uri = $this->getUriInfo(env('REQUEST_URI'), ['SeoMetaTags']);
        }

        $meta_tags_output = '';
        if($seo_uri !== NULL && is_array($seo_uri->meta_tags)) {
            foreach($seo_uri->meta_tags as $meta_tag){
                $meta_tags_output .= "<meta {$meta_tag->attribute1} {$meta_tag->attribute2}>\n";
            }
        }
        return $meta_tags_output;
    }

    public function checkUriInfo($uri, $page_title = 'マレーシアで求人-転職-就職-採用 |アイコニック - ICONIC Co., Ltd', $job = NULL, $search_keyword = NULL){

        //do not '.' character in the SEO uri
        if(strpos($uri, '.')){
            return NULL;
        }

        $seo_uris = TableRegistry::get('IconicSeo.SeoUris');
        $seo_uri = $seo_uris->find('all')
            ->where(['SeoUris.uri = ' => $uri])
            ->first();

        if($seo_uri === NULL){
            $default_description = "マレーシア, 求人, マレーシア, 求人, 仕事, 就職, 人材, 採用, アジア, 求人, 現地採用, 人材紹介, 転職";
            $default_keyword = 'マレーシア, 求人, 仕事,就職,人材, 採用, アジア求人, 現地採用, 人材紹介, 転職';

            if($search_keyword){
                $default_keyword .= ', ' . $search_keyword;
            }


            $new_uri = $seo_uris->newEntity([
                'uri'           =>  $uri,
                'type'          => 1,
                'created'       => date('Y-m-d H:i:s'),
                'modified'      => date('Y-m-d H:i:s'),
                'title'         => [
                    'title'     => $page_title,
                    'created'       => date('Y-m-d H:i:s'),
                    'modified'      => date('Y-m-d H:i:s')
                ],
                'canonical'     => [
                    'canonical' => (isset($_SERVER['HTTPS']) ? "https" : "http") . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
                    'is_active' => 1,
                    'created'       => date('Y-m-d H:i:s'),
                    'modified'      => date('Y-m-d H:i:s')
                ],
                'meta_tags'     => [
                    [
                        'attribute1_title'      => 'name',
                        'attribute1_content'    => 'keywords',
                        'attribute2_title'      => 'content',
                        'attribute2_content'    => $default_keyword
                    ],
                    [
                        'attribute1_title'      => 'name',
                        'attribute1_content'    => 'description',
                        'attribute2_title'      => 'content',
                        'attribute2_content'    => $default_description
                    ],
                    [
                        'attribute1_title'      => 'name',
                        'attribute1_content'    => 'robots',
                        'attribute2_title'      => 'content',
                        'attribute2_content'    => 'index, follow'
                    ],
                    [
                        'attribute1_title'      => 'property',
                        'attribute1_content'    => 'og:title',
                        'attribute2_title'      => 'content',
                        'attribute2_content'    => $page_title
                    ],
                    [
                        'attribute1_title'      => 'property',
                        'attribute1_content'    => 'og:description',
                        'attribute2_title'      => 'content',
                        'attribute2_content'    => $default_description
                    ],
                    [
                        'attribute1_title'      => 'property',
                        'attribute1_content'    => 'og:url',
                        'attribute2_title'      => 'content',
                        'attribute2_content'    => PROTOCOL . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']
                    ],
                    [
                        'attribute1_title'      => 'property',
                        'attribute1_content'    => 'og:image',
                        'attribute2_title'      => 'content',
                        'attribute2_content'    => PROTOCOL . "://{$_SERVER["HTTP_HOST"]}/img/ogp.png"
                    ]
                ]
            ],
                [ 'associated' => ['SeoTitles', 'SeoMetaTags', 'SeoCanonicals'] ]
            );

            if(is_null($job) && is_null($search_keyword)) {
                $seo_uris->save($new_uri);
                return $seo_uri;
            }
            return $new_uri;
        }
        return $seo_uri;
    }

    private function getUriInfo($uri, $contain = [], $condition = []){
        //check uri using regression
        $seo_uris = TableRegistry::get('IconicSeo.SeoUris');
        $seo_uri = $seo_uris->find('all')
            ->contain($contain)
            ->andWhere(array_merge([
                    'SeoUris.uri = ' => $uri,
                ], $condition)
            )->first();
        return $seo_uri;
    }



}