-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 27, 2015 at 02:55 PM
-- Server version: 5.5.43-0ubuntu0.14.04.1-log
-- PHP Version: 5.5.9-1ubuntu4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iconic_intl_additional`
--

-- --------------------------------------------------------

--
-- Table structure for table `cake_queue_phinxlog`
--

CREATE TABLE IF NOT EXISTS `cake_queue_phinxlog` (
  `version` bigint(20) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `iconic_seo_phinxlog`
--

CREATE TABLE IF NOT EXISTS `iconic_seo_phinxlog` (
  `version` bigint(20) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seo_canonical`
--

CREATE TABLE IF NOT EXISTS `seo_canonical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_uri_id` int(11) NOT NULL,
  `canonical` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seo_uri_id` (`seo_uri_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=163 ;

-- --------------------------------------------------------

--
-- Table structure for table `seo_meta_tags`
--

CREATE TABLE IF NOT EXISTS `seo_meta_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_uri_id` int(11) NOT NULL,
  `attribute1_title` varchar(255) DEFAULT NULL,
  `attribute1_content` varchar(255) DEFAULT NULL,
  `attribute2_title` varchar(255) DEFAULT NULL,
  `attribute2_content` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seo_uri_id` (`seo_uri_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1224 ;

-- --------------------------------------------------------

--
-- Table structure for table `seo_redirect`
--

CREATE TABLE IF NOT EXISTS `seo_redirect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_uri_id` int(11) NOT NULL,
  `redirect` varchar(255) NOT NULL,
  `type` char(3) NOT NULL DEFAULT '302',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seo_uri_id` (`seo_uri_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `seo_title`
--

CREATE TABLE IF NOT EXISTS `seo_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_uri_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seo_uri_id` (`seo_uri_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=173 ;

-- --------------------------------------------------------

--
-- Table structure for table `seo_uri`
--

CREATE TABLE IF NOT EXISTS `seo_uri` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) DEFAULT NULL,
  `is_approved` int(1) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uri_index` (`uri`),
  KEY `is_approved_index` (`is_approved`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=337 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_beanstalkd_job`
--

CREATE TABLE IF NOT EXISTS `t_beanstalkd_job` (
  `job_id` bigint(11) NOT NULL,
  `tube` varchar(255) NOT NULL,
  `class_name` varchar(255) NOT NULL,
  `class_method` varchar(255) NOT NULL,
  `data` mediumtext NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `t_queue_job`
--

CREATE TABLE IF NOT EXISTS `t_queue_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `job_data` text NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `comment` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id_index` (`job_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `seo_canonical`
--
ALTER TABLE `seo_canonical`
  ADD CONSTRAINT `seo_canonical_ibfk_1` FOREIGN KEY (`seo_uri_id`) REFERENCES `seo_uri` (`id`);

--
-- Constraints for table `seo_meta_tags`
--
ALTER TABLE `seo_meta_tags`
  ADD CONSTRAINT `seo_meta_tags_ibfk_1` FOREIGN KEY (`seo_uri_id`) REFERENCES `seo_uri` (`id`);

--
-- Constraints for table `seo_redirect`
--
ALTER TABLE `seo_redirect`
  ADD CONSTRAINT `seo_redirect_ibfk_1` FOREIGN KEY (`seo_uri_id`) REFERENCES `seo_uri` (`id`);

--
-- Constraints for table `seo_title`
--
ALTER TABLE `seo_title`
  ADD CONSTRAINT `seo_title_ibfk_1` FOREIGN KEY (`seo_uri_id`) REFERENCES `seo_uri` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
