# Search Engine Optimization CakePHP Plugin
* Author: BUI TRUNG TAN
* Version: 6.2.1
* License: MIT

## Features

Tool for all your CakePHP Search Engine Optimization needs
* Meta tags management
* Make sure all meta tags must be put into allowing list: Model/Behavior/Traits/SeoEscapeTrait.php function checkAttribute1Content