<?php
use Cake\Routing\Router;

Router::plugin('IconicSeo', function(\Cake\Routing\RouteBuilder $route) {
	$route->connect('/:controller/');
	$route->connect('/:controller/:action');
	$route->connect('/:controller/:action/*');
});