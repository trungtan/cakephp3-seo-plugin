<?php
use Phinx\Migration\AbstractMigration;

class CreateMMetaTags extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * http://book.cakephp.org/3.0/en/orm/schema-system.html
     * @return void
     *
     * Create tables with 'additional' data source:
     * bin/cake migrations migrate --plugin IconicSeo -c "additional"
     */
    public function change()
    {
        $table = $this->table('seo_uri', ['id' => true,'primary_key' => ['id']]);
        $table
            ->addColumn('uri', 'string', [
                'null' => true,
                'default' => null
            ])
            ->addColumn('type', 'integer', [
                'null' => false,
                'length' => 3,
                'default' => '1'
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex('uri', ['type' => 'index','unique' => true,'name' => 'uri_index'])
            ->addIndex('type', ['type' => 'index','unique' => false,'name' => 'type_index'])
            ->create();


        $table = $this->table('seo_meta_tags', ['id' => true,'primary_key' => ['id']]);
        $table
            ->addColumn('seo_uri_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('attribute1_title', 'string', [
                'null' => true,
                'default'   => null,
            ])
            ->addColumn('attribute1_content', 'string', [
                'null' => true,
                'default'   => null,
            ])
            ->addColumn('attribute2_title', 'string', [
                'null' => true,
                'default'   => null,
            ])
            ->addColumn('attribute2_content', 'string', [
                'null' => true,
                'default'   => null,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addForeignKey('seo_uri_id', 'seo_uri')
            ->create();

        $table = $this->table('seo_canonical', ['id' => true,'primary_key' => ['id']]);
        $table
            ->addColumn('seo_uri_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('canonical', 'string', [
                'null' => false,
            ])
            ->addColumn('is_active', 'integer', [
                'null' => false,
                'length' => 1,
                'default'  => '0'
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addForeignKey('seo_uri_id', 'seo_uri')
            ->create();

        $table = $this->table('seo_title', ['id' => true,'primary_key' => ['id']]);
        $table
            ->addColumn('seo_uri_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('title', 'string', [
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addForeignKey('seo_uri_id', 'seo_uri')
            ->create();

        $table = $this->table('seo_redirect', ['id' => true,'primary_key' => ['id']]);
        $table
            ->addColumn('seo_uri_id', 'integer', [
                'null' => false,
            ])
            ->addColumn('redirect', 'string', [
                'null' => false,
            ])
            ->addColumn('type', 'char', [
                'length' => 3,
                'default' => '302',
                'null' => false,
            ])
            ->addColumn('is_active', 'integer', [
                'null'  => false,
                'default'   => 1
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addForeignKey('seo_uri_id', 'seo_uri')
            ->create();
    }
}
