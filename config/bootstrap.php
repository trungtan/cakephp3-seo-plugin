<?php

use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * Comment the code below if you run:
        bin/cake migrations migrate --plugin IconicSeo -c "additional"
 * the first time
 *
 * Redirection should have a management.
 */

$seo_uris = TableRegistry::get('IconicSeo.SeoUris');

$seo_uri = $seo_uris->find('all')
    ->contain(['SeoRedirects'])
    ->andWhere([
        'SeoUris.uri = '            => env('REQUEST_URI'),
        'SeoRedirects.is_active = '    => 1,
    ])->first();

if($seo_uri !== NULL && $seo_uri->redirect !== NULL) {
    Router::redirect('*', $seo_uri->redirect->redirect, ['status' => $seo_uri->redirect->type]);
}

if(!defined('PROTOCOL')){
    if (isset($_SERVER['HTTPS'])) {
        define('PROTOCOL', 'https');
    } else {
        define('PROTOCOL', 'http');
    }
}
