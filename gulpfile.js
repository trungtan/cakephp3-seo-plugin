'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('scss:main',function(){
    console.log('==================== Gulp sass is starting! =====================');
    return gulp.src('./webroot/scss/*.scss').
        pipe(sass.sync().on('error', sass.logError)).
        pipe(sass( {})).
        pipe(gulp.dest('./webroot/css/'));
});

gulp.task('scss-watch', function () {
  gulp.watch('./webroot/scss/*.scss', ['scss:main']);
});
