-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 14, 2016 at 10:48 AM
-- Server version: 5.6.29-log
-- PHP Version: 7.0.3

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iconic_my_additional`
--

-- --------------------------------------------------------

--
-- Table structure for table `seo_canonical`
--

DROP TABLE IF EXISTS `seo_canonical`;
CREATE TABLE `seo_canonical` (
  `id` int(11) NOT NULL,
  `seo_uri_id` int(11) NOT NULL,
  `canonical` varchar(255) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seo_footer_keywords`
--

DROP TABLE IF EXISTS `seo_footer_keywords`;
CREATE TABLE `seo_footer_keywords` (
  `id` int(11) NOT NULL,
  `seo_uri_id` int(11) NOT NULL,
  `ja_keywords` text,
  `en_keywords` text,
  `vi_keywords` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seo_meta_tags`
--

DROP TABLE IF EXISTS `seo_meta_tags`;
CREATE TABLE `seo_meta_tags` (
  `id` int(11) NOT NULL,
  `seo_uri_id` int(11) NOT NULL,
  `attribute1_title` varchar(255) DEFAULT NULL,
  `attribute1_content` varchar(255) DEFAULT NULL,
  `attribute2_title` varchar(255) DEFAULT NULL,
  `attribute2_content` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seo_redirect`
--

DROP TABLE IF EXISTS `seo_redirect`;
CREATE TABLE `seo_redirect` (
  `id` int(11) NOT NULL,
  `seo_uri_id` int(11) NOT NULL,
  `redirect` varchar(255) NOT NULL,
  `type` char(3) NOT NULL DEFAULT '302',
  `is_active` int(11) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seo_title`
--

DROP TABLE IF EXISTS `seo_title`;
CREATE TABLE `seo_title` (
  `id` int(11) NOT NULL,
  `seo_uri_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seo_top_messages`
--

DROP TABLE IF EXISTS `seo_top_messages`;
CREATE TABLE `seo_top_messages` (
  `id` int(11) NOT NULL,
  `seo_uri_id` int(11) NOT NULL,
  `en_top_message` varchar(255) DEFAULT NULL,
  `vi_top_message` varchar(255) DEFAULT NULL,
  `ja_top_message` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `seo_uri`
--

DROP TABLE IF EXISTS `seo_uri`;
CREATE TABLE `seo_uri` (
  `id` int(11) NOT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `type` tinyint(3) NOT NULL DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `seo_canonical`
--
ALTER TABLE `seo_canonical`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seo_uri_id` (`seo_uri_id`);

--
-- Indexes for table `seo_footer_keywords`
--
ALTER TABLE `seo_footer_keywords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_meta_tags`
--
ALTER TABLE `seo_meta_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seo_uri_id` (`seo_uri_id`);

--
-- Indexes for table `seo_redirect`
--
ALTER TABLE `seo_redirect`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seo_uri_id` (`seo_uri_id`);

--
-- Indexes for table `seo_title`
--
ALTER TABLE `seo_title`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seo_uri_id` (`seo_uri_id`);

--
-- Indexes for table `seo_top_messages`
--
ALTER TABLE `seo_top_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo_uri`
--
ALTER TABLE `seo_uri`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uri_index` (`uri`),
  ADD KEY `is_approved_index` (`type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `seo_canonical`
--
ALTER TABLE `seo_canonical`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4282;
--
-- AUTO_INCREMENT for table `seo_footer_keywords`
--
ALTER TABLE `seo_footer_keywords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=274;
--
-- AUTO_INCREMENT for table `seo_meta_tags`
--
ALTER TABLE `seo_meta_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30421;
--
-- AUTO_INCREMENT for table `seo_redirect`
--
ALTER TABLE `seo_redirect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `seo_title`
--
ALTER TABLE `seo_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4342;
--
-- AUTO_INCREMENT for table `seo_top_messages`
--
ALTER TABLE `seo_top_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=281;
--
-- AUTO_INCREMENT for table `seo_uri`
--
ALTER TABLE `seo_uri`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5251;SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
